################## Import Modules
try:
    import requests
    import os
    import json
    import random
    import traceback
    import datetime
    import time
    import ctypes
    from colorama import Fore
except:
    print("Failed to import modules. Trying to manually install it")
    try:
        import os
        os.system("pip install requests")
        os.system("pip install colorama")
        print("Installed Modules")
        import requests
        import colroama
    except:
        print("failed to install modules sad")
################## Define Variables and Load Configs and set title
try:
    ctypes.windll.kernel32.SetConsoleTitleW(f"TradeBot V1.5")
    os.system("cls")
except:
    print('\33]0;TradeBot V1.5\a', end='')
projectedindice = 19
bestprice = 5
value = 16
origprice = 2
rap = 22
config = json.load(open("config.json", encoding="UTF-8"))
cookie = config['cookie']
flip_projected = config['projected_flipper']
min_profit = config['min_profit']
max_profit = config['max_profit']
SendTradeToSameUser = config['SendTradeToSameUser']
profit_method = config['profit_method'].lower()
if profit_method == "value":
    rap = 16
else:
    rap = 22
non_trade = config['ItemsYouWantToTradeAway']
dont_want = config['ItemsYouDontWant']
################## Classes
class TradeBot:
    def __init__(self,cookie):
        self.ses = requests.Session()
        self.ses.cookies['.ROBLOSECURITY'] = cookie
        self.item_data = {}
        self.traders = []
        self.useritems = []
        self.userassetids = {}
        self.userprojected = []
        login = self.ses.get("https://users.roblox.com/v1/users/authenticated")
        if login.status_code==200:
            self.username = login.json()['name']
            self.userid = login.json()['id']
            self.works = True
            print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Logged In | UserName: {Fore.WHITE}{self.username}")
        else:
            print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Invalid Cookie{Fore.WHITE}")
    def update_inventory(self):
        userinventory = self.ses.get(f"https://inventory.roblox.com/v1/users/{self.userid}/assets/collectibles?cursor=").json()
        for x in userinventory['data']:
            self.userassetids[str(x['assetId'])] = x['userAssetId']
            x = str(x['assetId'])
            if int(x) not in non_trade:
                if self.item_data[x][projectedindice] == 1:
                    self.userprojected.append(x)
                    print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Loaded Projected: {Fore.WHITE}{x}")
                else:
                    self.useritems.append(x)
                    print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Loaded: {Fore.WHITE}{x}")
    def update_data(self):
        text = self.ses.get(f"https://www.rolimons.com/catalog").text
        data = json.loads(text.split("var item_details = ")[1].split(";")[0])
        for x in data:
            if data[x][2] == None:
                data[x][2] == 0
            if data[x][16] == None:
                data[x][16] = 0
        self.item_data = data
    def get_users(self):
        f = self.ses.get("https://catalog.roblox.com/v1/search/items?category=Collectibles&limit=100&sortAggregation=5&sortType=2&subcategory=Collectibles").json()['data']
        resellers = self.ses.get(f"https://economy.roblox.com/v1/assets/{random.choice(f)['id']}/resellers?cursor=&limit=100").json()['data']
        for x in resellers:
            self.traders.append(x['seller']['id'])
        print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Loaded: {Fore.WHITE}{len(self.traders)} {Fore.LIGHTBLACK_EX}Traders")
    def trader(self,traderid,itemsr,itemsg):
        xsrf=self.ses.post("https://trades.roblox.com/v1/trades/send",headers={"X-CSRF-TOKEN":""}).headers['x-csrf-token']
        trade=self.ses.post("https://trades.roblox.com/v1/trades/send",headers={"X-CSRF-TOKEN":xsrf},json={"offers":[{"userId":traderid,"userAssetIds":itemsr,"robux":None},{"userId":self.userid,"userAssetIds":itemsg,"robux":None}]})
        return trade
    def returntrade(self,trader_inv,traderid):
        totalprofit = 0
        gibitems = []
        returnitems=[]
        for u in self.useritems:
            if len(gibitems) > 4:
                break
            for t in trader_inv:
                s = str(t['assetId'])
                profit = ((self.item_data[s][rap] - self.item_data[u][rap])/self.item_data[s][rap])*100
                abc = profit < max_profit and profit > min_profit
                if self.item_data[s][rap] > self.item_data[u][rap] and self.item_data[s][projectedindice] == None and self.item_data[s][origprice] > self.item_data[u][origprice] and abc:
                    totalprofit+=self.item_data[s][rap]
                    totalprofit-=self.item_data[u][rap]
                    trader_inv.remove(t)
                    returnitems.append(t['userAssetId'])
                    gibitems.append(self.userassetids[u])
                    break
        if returnitems and gibitems:
            profit=round(totalprofit)
            return([self.trader(traderid,returnitems,gibitems),profit])
        else:
            return None

    def valid_trader(self,id):
        maxload = 15 #max pages to load
        cursor = ""
        traderinvv = []
        check=self.ses.get(f"https://trades.roblox.com/v1/users/{id}/can-trade-with").json()['canTrade']
        lastonline = self.ses.get(f"https://api.roblox.com/users/{id}/onlinestatus/").json()['LastOnline'].split("T")[0].split("-")
        startDate = datetime.datetime.utcnow().isoformat() + "Z"
        todaydate = startDate.split("T")[0].split("-")
        day = int(startDate.replace("T","-").split("-")[2])
        lastonlinedat = int(lastonline[2])
        if check and todaydate[0] == lastonline[0] and todaydate[1] == lastonline[1] and day - lastonlinedat < 2:
            trader_inv = self.ses.get(f"https://inventory.roblox.com/v1/users/{id}/assets/collectibles?cursor={cursor}")
            if trader_inv.status_code == 200:
                cursor = trader_inv.json()['nextPageCursor']
                for x in trader_inv.json()['data']:
                    traderinvv.append(x)
                while cursor != None:
                    if maxload == 0:
                        break
                    maxload-=1
                    trader_inv = self.ses.get(f"https://inventory.roblox.com/v1/users/{id}/assets/collectibles?cursor={cursor}").json()
                    cursor = trader_inv['nextPageCursor']
                    data = trader_inv['data']
                    for x in data:
                        traderinvv.append(x)
            else:
                return False
        else:
            return False
        return traderinvv
    def worker(self):
        while self.traders:
            trader = self.traders.pop()
            trader_inv = self.valid_trader(trader)
            if trader_inv:
                print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Loaded{Fore.WHITE} {len(trader_inv)}{Fore.LIGHTBLACK_EX} Items")
                for f in trader_inv:
                    if self.userprojected and flip_projected:
                        projected = random.choice(self.userprojected)
                        projecteddata = self.item_data[projected]
                        x = str(f['assetId'])
                        if int(x) not in dont_want:
                            if projecteddata[rap] > self.item_data[x][rap] and self.item_data[x][projectedindice] == None:
                                tradesent = self.trader(trader,[f['userAssetId']],[self.userassetids[projected]])
                                if tradesent.status_code == 200:
                                    print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Flipping Projected | Sent/Recieve: {Fore.WHITE}{projecteddata[rap]}/{self.item_data[x][rap]}")
                                    if not SendTradeToSameUser:
                                        break
                                elif tradesent.status_code == 429:
                                    print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}TimeLimited{Fore.WHITE}")
                                    time.sleep(1800)
                                elif "privacy" in tradesent.text.lower():
                                    print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}User's Privacy doesnt allow trades{Fore.WHITE}")
                                    break
                if self.useritems:
                    tradeeeee=self.returntrade(trader_inv,trader)
                    if tradeeeee != None:
                        tradesent = tradeeeee[0]
                        if tradesent.status_code == 200:
                            print(f"{Fore.WHITE}[ {Fore.GREEN}+ {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Sending Trade | Profit(Robux): {Fore.WHITE}{round(tradeeeee[1])}")
                        elif tradesent.status_code == 429:
                            print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}TimeLimited{Fore.WHITE}")
                            time.sleep(60)
                        elif "privacy" in tradesent.text.lower():
                            print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}User's Privacy doesnt allow trades{Fore.WHITE}")
                            break
                        else:
                            print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Failed to send Trade")
                    else:
                        print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}User has nothing good. LOL What a looser")
            else:
                print(f"{Fore.WHITE}[ {Fore.RED}- {Fore.WHITE}] {Fore.LIGHTBLACK_EX}Private Inventory. Id: {Fore.WHITE}{trader} {Fore.LIGHTBLACK_EX}Left: {Fore.WHITE}{len(self.traders)}")

#######################Usage dumbasses
if __name__ == '__main__':
    while True:
        try:
            ses = TradeBot(cookie)
            ses.update_data()
            ses.update_inventory()
            ses.get_users()
            ses.worker()
        except Exception as e:
            open("debug.txt","a").write(f"{e}\n")
            traceback.print_exc()
